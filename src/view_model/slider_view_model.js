export default class SliderViewModel {
  constructor(initialValue, mainViewModel) {
    this.m_initial_value = initialValue;
    this.m_mainViewModel = mainViewModel;

    this.setSliderText(initialValue);

    const slider = document.getElementById(`slider`);
    slider.min = 2;
    slider.max = 100;
    slider.value = this.m_initial_value;

    const _this = this;
    const _this2 = this;
    slider.addEventListener(`input`, function(event) {
      const value = event.target.value;

      _this.setSliderText(value);

      _this2.m_mainViewModel.redrawTheGrid(value);
    });
  }

  setSliderText(value) {
    const sliderText = document.getElementById(`sliderText`);
    sliderText.innerHTML = `Grid size  ${value} x ${value}`;
  }
}
