export default class MainViewModel {
  constructor(size) {
    this.m_size = size;
  }

  redrawTheGrid(size) {
    this.m_size = size;
    this.clearTheGrid();
    this.drawTheGrid();
  }

  clearTheGrid() {
    document.getElementById(`div-right`).innerHTML = ``;
  }

  drawTheGrid() {
    for (let i = 0; i < this.m_size - 1; i++) {
      this.addRow(`row-item`);
    }
    this.addRow(`last-row-item`);

    this.fixLastItemInLastRow();
  }

  addRow(cssRowItemClas) {
    const row = document.createElement(`div`);
    row.classList.add(`row`);

    for (let i = 0; i < this.m_size - 1; i++) {
      this.addRowItem(row, cssRowItemClas);
    }
    this.addRowItem(row, `row-last-item`);

    document.getElementById(`div-right`).appendChild(row);
  }

  addRowItem(row, cssClassName) {
    const rowItem = document.createElement(`div`);
    rowItem.classList.add(cssClassName);
    row.appendChild(rowItem);

    rowItem.addEventListener(`mouseenter`, function(event) {
      event.target.style.background = `black`;
    });
  }

  fixLastItemInLastRow() {
    const lastRowItems = document.getElementsByClassName(`row-last-item`);
    lastRowItems[this.m_size - 1].className = `last-row-last-item`;
  }
}
