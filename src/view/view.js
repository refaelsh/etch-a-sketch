import MainViewModel from "../view_model/main_view_model.js";
import SliderViewModel from "../view_model/slider_view_model.js";

const size = 50;

const mainViewModel = new MainViewModel(size);
mainViewModel.drawTheGrid();

const sliderViewModel = new SliderViewModel(size, mainViewModel);

const btnClear = document.getElementById(`btnClear`);
btnClear.addEventListener(`click`, function(event) {
  const slider = document.getElementById(`slider`);
  mainViewModel.redrawTheGrid(slider.value);
});
